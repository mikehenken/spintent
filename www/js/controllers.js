angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $http, SpintentData, $ionicLoading) {

	$scope.$on('$ionicView.enter', function() {
		$scope.sendAbility = SpintentData.check();
    $scope.data = SpintentData.get();
	});

	$scope.sendData = function(){
		SpintentData.send();
    $ionicLoading.show({
      template: 'Data Sending...',
      duration: 3000
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
	}

	$scope.updateData = function(){
		SpintentData.update();
		$scope.data = SpintentData.get();
	}

})

.controller('AccountCtrl', function($scope, SpintentData) {
  
  $scope.settings = {
    enable: SpintentData.check()
  };

  $scope.toggleDataSend = function(){
  	if($scope.settings.enable)
  		SpintentData.on();
  	else
  		SpintentData.off();
  }

});
