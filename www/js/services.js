angular.module('starter.services', [])

.factory('SpintentData', function($http) {
    var UpdateData = function(callback){
        var date = new Date();
        var json = {
            "device"          : { 
                "Device Missing" : "No Information Available"
            },
            "referrer"        : "No Data Found", 
            "updated"         : date.toLocaleString()
        };
        if(window.device){
            json.device = {
              "model"         : window.device.model, 
              "cordova"       : window.device.cordova, 
              "platform"      : window.device.platform, 
              "version"       : window.device.version, 
              "UUID"          : window.device.uuid, 
              "manufacturer"  : window.device.name
            };
        }
        //Make sure that the device is ready and then look for the referrer2
        if (window.cordova) {
              document.addEventListener("deviceready", GetIntents, false);
              function GetIntents(){
                  window.applicationPreferences.get("referrer", function(value) {
                      json.referrer = value;
                      console.log("[appintent] Success! " + JSON.stringify(json));
                      window.localStorage.setItem('spintent-install', JSON.json(json));
                      return json;
                  }, function(error) {
                      console.log("[appintent] Error! " + JSON.stringify(error));
                      window.localStorage.setItem('spintent-install', JSON.stringify(json));
                      return json;
                  });
              }
        } 
        else {
            return json;
        }
  }

  var GetData = function(reset){
    if (window.localStorage.getItem("spintent-install") === null || (typeof reset !== 'undefined' && reset == true)) {
        console.log('set to localstorage');
        return UpdateData();
    }
    else {
        console.log('get from localstorage', JSON.parse( window.localStorage.getItem("spintent-install")));
        return JSON.parse( window.localStorage.getItem("spintent-install"));
    }
  }

  return {
    send: function(reset) {
        reset = (typeof reset !== 'undefined') ? reset : false;
        var data = GetData(reset);
        var link = 'https://api.alltheapps.org/mobile/install';
        $http.post(link, data).then(function (res){
            console.log("[appintent] Post Data: " + JSON.stringify(res.data))
        });
    },

    get: function(reset){
      reset = (typeof reset !== 'undefined') ? reset : false;
      console.log('from actual return SendData obj', GetData(reset))
      return GetData(reset);
    },

    update: function(){
      return UpdateData();
    },

    on: function(){
      window.localStorage.setItem("spintent-data-send", "true")
    },

    off: function(){
      window.localStorage.setItem("spintent-data-send", "false")
    },

    check: function(){
       return  (window.localStorage.getItem("spintent-data-send") === null || window.localStorage.getItem("spintent-data-send") != "false") ? true : false;
    }
  };
});
